package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.controller.commands.CtrlTackCmd;
import fr.istic.aoc.metronome.controller.commands.CtrlTickCmd;
import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.model.Clock;
import fr.istic.aoc.metronome.model.Core;
import fr.istic.aoc.metronome.model.interfaces.IClock;
import fr.istic.aoc.metronome.model.interfaces.ICore;

/**
 * Metronome App
 *
 */
public class CoreRun {

    public static void main(String[] args) {
        // Init clock and modules
        IClock clock = new Clock();
        ICore core = new Core(clock, 80, 4, true);
        IController ctrl = new Controller(core);

        // Define all commands links
        core.setCtrlTickCommand(new CtrlTickCmd(ctrl));
        core.setCtrlTackCommand(new CtrlTackCmd(ctrl));
    }

}
