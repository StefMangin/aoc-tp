package fr.istic.aoc.metronome.controller.commands;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.utils.Command;

/**
 * Created by stephane on 21/10/15.
 */
public class CtrlTickCmd implements Command {

    private IController ctrl;

    public CtrlTickCmd(IController ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void execute() {
        ctrl.onTick();
    }

    @Override
    public void run() {
        execute();
    }
}
