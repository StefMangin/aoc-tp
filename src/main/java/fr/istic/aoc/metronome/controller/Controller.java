package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.utils.Command;
import fr.istic.aoc.metronome.model.interfaces.ICore;

import java.util.Observable;
import java.util.logging.Logger;

/**
 * Created by stephane on 29/09/15.
 */
public class Controller implements IController {

    private Logger log = Logger.getLogger(getClass().getName());
    protected Command tackCmd;
    protected Command tickCmd;
    protected ICore core;

    public Controller(ICore core) {
        this.core = core;
    }

    @Override
    public Integer showTempo() {
        return core.getTempo();
    }

    @Override
    public Integer showBpm() {
        return core.getBpm();
    }

    @Override
    public String showLogs() {
        return core.getLogs();
    }

    @Override
    public void onTick() {
        log.finest("Tick on controller");
        tickCmd.execute();
    }

    @Override
    public void onTack() {
        log.finest("Tack on controller");
        tackCmd.execute();
    }

    @Override
    public void updateBpm(Integer bpm) {
        core.setBpm(bpm);
    }

    @Override
    public void updateTempo(Integer tempo) {
        core.setTempo(tempo);
    }

    @Override
    public void increaseBpm() {
        core.increaseBpm();
    }

    @Override
    public void decreaseBpm() {
        core.decreaseBpm();
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    @Override
    public void setTackCmd(Command tackCmd) {
        this.tackCmd = tackCmd;
    }

    @Override
    public void setTickCmd(Command tickCmd) {
        this.tickCmd = tickCmd;
    }

    @Override
    public void start() {
        core.setOn(true);
    }

    @Override
    public void stop() {
        core.setOn(false);
    }

    @Override
    public ICore getCore() {
        return core;
    }

}
