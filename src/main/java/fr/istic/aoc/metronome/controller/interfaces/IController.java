package fr.istic.aoc.metronome.controller.interfaces;

import fr.istic.aoc.metronome.model.interfaces.ICore;
import fr.istic.aoc.metronome.utils.Command;

import java.util.Observer;

/**
 * Created by stephane on 06/10/15.
 */
public interface IController extends Observer{

    /**
     * Launch the tempo signal
     *
     * @return
     */
    Integer showTempo();

    /**
     * Launch the bpm signal
     *
     * @return
     */
    Integer showBpm();

    String showLogs();

    void onTick();
    void onTack();

    /**
     * Update the bpm
     *
     * @param bpm
     */
    void updateBpm(Integer bpm);

    /**
     * Update the tempo
     *
     * @param tempo
     */
    void updateTempo(Integer tempo);

    /**
     * Increase the bpm value
     */
    void increaseBpm();

    /**
     * Decrease the bpm value
     */
    void decreaseBpm();

    /**
     * Define the tack command
     *
     * @param tackCmd
     */
    void setTackCmd(Command tackCmd);

    /**
     * Define the tick signal
     *
     * @param tickCmd
     */
    void setTickCmd(Command tickCmd);

    /**
     * Send the start signal to the core
     */
    void start();

    /**
     * Send the stop signal to the core
     */
    void stop();

    ICore getCore();
}
