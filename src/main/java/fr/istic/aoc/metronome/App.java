package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.controller.commands.CtrlTackCmd;
import fr.istic.aoc.metronome.controller.commands.CtrlTickCmd;
import fr.istic.aoc.metronome.model.Clock;
import fr.istic.aoc.metronome.model.Core;
import fr.istic.aoc.metronome.view.ControllerFX;
import fr.istic.aoc.metronome.view.commands.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Metronome App
 *
 */
public class App extends Application {

    public static void main(String[] args) {
        // launch JavaFX application
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/metronome.fxml"));
        Parent page = loader.load();

        // Configure scene
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.setTitle("Metronome - v1");
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.setImplicitExit(false);
                stage.close();

            }
        });

        // Start the view
        stage.show();

        // Init clock and modules
        Clock clock = new Clock();
        Core core = new Core(clock, 80, 4, false);
        Controller ctrl = new Controller(core);
        ControllerFX ctrlFX = loader.getController();
        ctrlFX.setController(ctrl);
        core.addObserver(ctrlFX);

        // Define all commands links
        core.setCtrlTickCommand(new CtrlTickCmd(ctrl));
        core.setCtrlTackCommand(new CtrlTackCmd(ctrl));
        ctrl.setTickCmd(new TickCmd(ctrlFX));
        ctrl.setTackCmd(new TackCmd(ctrlFX));
    }
}
