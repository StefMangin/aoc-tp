package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.model.interfaces.IClock;
import fr.istic.aoc.metronome.utils.Command;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by stephane on 06/10/15.
 */
public class Clock implements IClock {

    protected static ScheduledExecutorService threadPool;
    protected Command cmd;
    protected Long period;
    protected Logger log = Logger.getLogger(getClass().getName());
    protected Boolean previousOn = false;

    @Override
    public void addPeriodCmd(Command cmd, Long p) {
        log.fine("Clock configured: delay=" + p + ", command=" + cmd);
        this.cmd = cmd;
        this.period = p;
    }

    @Override
    public void addDelayCmd(Command c, Long p) {
        this.cmd = c;
        this.period = p;
        threadPool = Executors.newScheduledThreadPool(1);
        threadPool.scheduleWithFixedDelay(cmd::execute, period, 0, TimeUnit.MILLISECONDS);
    }

    @Override
    synchronized public void updatePeriod(Long period) {
        stop();
        this.period = period;
        start();
    }

    @Override
    synchronized public void start() {
        log.info("Clock started !");
        if (!previousOn) {
            threadPool = Executors.newScheduledThreadPool(1);
            threadPool.scheduleAtFixedRate(cmd::execute, 0, period, TimeUnit.MILLISECONDS);
        }
        previousOn = true;
    }

    @Override
    synchronized public void stop() {
        log.info("Clock stopped !");
        if (previousOn) {
            threadPool.shutdownNow();
        }
        previousOn = false;
    }
}
