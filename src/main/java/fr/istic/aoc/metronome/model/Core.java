package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.model.commands.CoreTickCmd;
import fr.istic.aoc.metronome.model.interfaces.IClock;
import fr.istic.aoc.metronome.model.interfaces.ICore;
import fr.istic.aoc.metronome.utils.Command;

import java.util.Observable;
import java.util.logging.Logger;

/**
 * Created by stephane on 06/10/15.
 */
public class Core extends Observable implements ICore {

    private IClock clock;
    private Boolean on = false; // Should be initialized
    private Integer bpm;
    private Integer tempo;
    private int currentBit; // An integer overflow could happen, far, far away on method onTick
    private Command ctrlTackCmd;
    private Command ctrlTickCmd;
    private Logger log = Logger.getLogger(getClass().getName());
    StringBuilder logs = new StringBuilder();
    private boolean toUpdate = false; // Used to update clock on an onTick signal

    public Core(IClock clock, Integer tempo, Integer bpm, Boolean autostart) {
        this.tempo = tempo;
        this.bpm = bpm;
        currentBit = 0;
        this.clock = clock;
        log.info("Core initialized");
        setOn(autostart);
    }

    /* Return the click delay in milliseconds
     *
     */
    private Long relativeTempo() {
        return (long) ((60.0 / tempo) * 1000);
    }

    @Override
    public Integer getBpm() {
        return bpm;
    }

    private Boolean checkBpm(Integer bpm) {
        if (bpm < 2) {
            log.severe("Bpm value can't be under 2.");
            logs.append("Bpm value can't be under 2. \n");
            return false;
        } else if (bpm > 7) {
            log.severe("Bpm value can't be over 8.");
            logs.append("Bpm value can't be over 8. \n");
            return false;
        }
        return true;
    }

    private Boolean checkTempo(Integer tempo) {
        if (tempo < 20) {
            log.severe("Tempo value can't be under 20.");
            logs.append("Tempo value can't be under 20. \n");
            return false;
        }
        else if (tempo > 280) {
            log.severe("Tempo value can't be over 280.");
            logs.append("Tempo value can't be over 280. \n");
            return false;
        }
        return true;
    }

    @Override
    public void setBpm (Integer bpm) {
        if (checkBpm(bpm)) {
            this.bpm = bpm;
            toUpdate = true;
            log.info("Core bpm changed");
            log.fine("# Tempo => " + tempo);
            log.fine("# Beats per mesure => " + bpm);
            log.fine("# Second per beat => " + 60.0 / tempo);
            log.fine("# Millisecond per beat => " + (60.0 / tempo) * 1000);
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public Integer getTempo() {
        return tempo;
    }

    @Override
    public void setTempo(Integer tempo) {
        if (checkTempo(tempo)) {
            this.tempo = tempo;
            toUpdate = true;
            log.info("Core tempo changed");
            log.fine("# Tempo => " + tempo);
            log.fine("# Beats per mesure => " + bpm);
            log.fine("# Second per beat => " + 60.0 / tempo);
            log.fine("# Millisecond per beat => " + (60.0 / tempo) * 1000);
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public Boolean getOn() {
        return on;
    }

    @Override
    public void setOn(Boolean on) {
        if (this.on != on) {
            if (on) {
                clock.addPeriodCmd(new CoreTickCmd(this), relativeTempo());
                clock.start();
            } else {
                clock.stop();
            }
            log.info("Core state changed : " + this.on);
            this.on = on;
            this.currentBit = 0; // Don't forget to reinit
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public void onTick() {
        if (on) {
            // Update is done here to be synchronized with the clock
            if (toUpdate) {
                clock.updatePeriod(relativeTempo());
                toUpdate = false;
            }
            if ((currentBit % bpm) == 0) {
                ctrlTickCmd.execute();
            } else {
                ctrlTackCmd.execute();
            }
            currentBit++;
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public void setCtrlTickCommand(Command tick) {
        this.ctrlTickCmd = tick;
    }

    @Override
    public void setCtrlTackCommand(Command tack) {
        this.ctrlTackCmd = tack;
    }

    @Override
    public void increaseBpm() {
        setBpm(bpm+1);
    }

    @Override
    public void decreaseBpm() {
        setBpm(bpm-1);
    }

    @Override
    public String getLogs() {
        return logs.toString();
    }

    @Override
    public IClock getClock() {
        return this.clock;
    }

}
