package fr.istic.aoc.metronome.model.interfaces;

import fr.istic.aoc.metronome.utils.Command;

/**
 * Created by stephane on 06/10/15.
 */
public interface IClock {

    /**
     * Define the command and the delay to be executed periodically
     *
     * @param c Command to execute
     * @param p Period in millisecond
     */
    void addPeriodCmd(Command c, Long p);

    /**
     * Define the command and the delay to be executed after
     *
     * @param c Command to execute
     * @param p Delay in millisecond
     */
    void addDelayCmd(Command c, Long p);

    /**
     * Update the period
     *
     * If clock is on, restart.
     * If clock is off, does not start.
     *
     * @param period Period in millisecond
     */
    void updatePeriod(Long period);

    /**
     * Start the command calling scheduler
     */
    void start();

    /**
     * Stop the command calling scheduler
     */
    void stop();

}
