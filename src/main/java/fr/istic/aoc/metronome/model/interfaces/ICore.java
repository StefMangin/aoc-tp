package fr.istic.aoc.metronome.model.interfaces;

import fr.istic.aoc.metronome.controller.commands.CtrlTackCmd;
import fr.istic.aoc.metronome.controller.commands.CtrlTickCmd;
import fr.istic.aoc.metronome.utils.Command;

/**
 * Created by stephane on 06/10/15.
 */
public interface ICore {

    /**
     * Get the number of bit per mesure (ex: 4)
     *
     * @return int
     */
    Integer getBpm();

    /**
     * Set the number of bit per mesure (ex: 4)
     *
     * @param bpm int
     */
    void setBpm(Integer bpm);

    /**
     * Get the global tempo (ex: 80)
     *
     * @return int
     */
    Integer getTempo();

    /**
     * Set the tempo (ex: 80)
     *
     * @param tempo int
     */
    void setTempo(Integer tempo);

    /**
     * Get the current state of the core
     *
     * @return bool
     */
    Boolean getOn();

    /**
     * Set the current state of the core
     *
     * @param on bool
     */
    void setOn(Boolean on);

    /** Launch the beat signal
     *
     */
    void onTick();

    /** Define the tick command to call
     *
     * @param tick CtrlTickCmd
     */
    void setCtrlTickCommand(Command tick);

    /** Define the tack command to call
     *
     * @param tack CtrlTackCmd
     */
    void setCtrlTackCommand(Command tack);

    /**
     * Increase the bpm value
     */
    void increaseBpm();

    /**
     * Decrease the bpm value
     */
    void decreaseBpm();

    /**
     * Get the information outputs
     *
     * @return String
     */
    String getLogs();

    IClock getClock();
}
