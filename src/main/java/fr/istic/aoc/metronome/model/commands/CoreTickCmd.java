package fr.istic.aoc.metronome.model.commands;

import fr.istic.aoc.metronome.model.interfaces.ICore;
import fr.istic.aoc.metronome.utils.Command;

import java.util.logging.Logger;

/**
 * Created by stephane on 21/10/15.
 */
public class CoreTickCmd implements Command {

    private ICore core;
    private Logger log = Logger.getLogger(getClass().getName());

    public CoreTickCmd(ICore core) {
        this.core = core;
    }

    @Override
    public void execute() {
        log.fine("CoreTickCmd called.");
        core.onTick();
    }

    @Override
    public void run() {
        execute();
    }
}
