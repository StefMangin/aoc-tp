package fr.istic.aoc.metronome.hardware;

import fr.istic.aoc.metronome.hardware.component.*;

/**
 * Created by blacknight on 24/01/16.
 */
public interface IHardware {

    Afficheur getAfficheur();
    Clavier getClavier();
    EmetteurSonore getEmetteurSonore();
    Horloge getHorloge();
    Molette getMolette();

}
