package fr.istic.aoc.metronome.hardware.component;

/**
 * Created by pollt on 1/23/16.
 */
public interface EmetteurSonore {
    void emettreClic();
}
