package fr.istic.aoc.metronome.hardware.component;

/**
 * Created by pollt on 1/23/16.
 */
public interface Clavier {
    // Retourne true si le bouton i est enfoncé, false si il est relaché.
// Le n°1 est start, le 2 est stop, le 3 est inc et le 4 est dec.
    boolean touchePressée(int i);
}
