package fr.istic.aoc.metronome.hardware.component;

/**
 * Created by pollt on 1/23/16.
 */
public interface Molette {
    // Retourne la position de la molette, entre 0.0 et 1.0,
    // en virgule flottante
    float position();
}
