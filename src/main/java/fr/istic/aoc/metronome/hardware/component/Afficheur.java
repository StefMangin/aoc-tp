package fr.istic.aoc.metronome.hardware.component;

/**
 * Created by pollt on 1/23/16.
 */
public interface Afficheur {
    // Contrôle des LEDs
    void allumerLED(int numLED);

    void éteindreLED(int numLED);

    // Affiche un entier sur l’afficheur externe du métronome
    void afficherTempo(int valeurTempo);
}
