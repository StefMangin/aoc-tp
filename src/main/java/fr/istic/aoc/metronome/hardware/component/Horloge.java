package fr.istic.aoc.metronome.hardware.component;

/**
 * Created by pollt on 1/23/16.
 */
public interface Horloge {
    // Appel périodique de l’opération execute() de cmd,
// toutes les périodeEnSecondes secondes,
// avec une précision d’une milliseconde.
    void activerPériodiquement(Command cmd,
                               float périodeEnSecondes);

    // Appel de l’opération execute() de cmd,
// après un délai de délaiEnSecondes secondes,
// avec une précision d’une milliseconde.
    void activerAprèsDélai(Command cmd, float délaiEnSecondes);

    void désactiver(Command cmd);
}
