package fr.istic.aoc.metronome.view.hardwareAdapters;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.hardware.component.EmetteurSonore;
import fr.istic.aoc.metronome.view.ControllerFX;

/**
 * Created by pollt on 1/23/16.
 */
public class EmetteurSonoreAdapter extends ControllerFX implements EmetteurSonore {

    @Override
    public void emettreClic() {
        tick.stop();
        tick.play();
    }
}
