package fr.istic.aoc.metronome.view.hardwareAdapters;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.hardware.component.Molette;
import fr.istic.aoc.metronome.view.ControllerFX;

/**
 * Created by pollt on 1/23/16.
 */
public class MoletteAdapter extends ControllerFX implements Molette {

    @Override
    public float position() {
        return (float)tempoSlider.getValue();
    }
}
