package fr.istic.aoc.metronome.view.hardwareAdapters;

import fr.istic.aoc.metronome.hardware.component.Afficheur;
import fr.istic.aoc.metronome.view.ControllerFX;

/**
 * Created by blacknight on 24/01/16.
 */
public class AfficheurAdapter extends ControllerFX implements Afficheur {

    @Override
    public void allumerLED(int numLED) {
        this.tack.play();
    }

    @Override
    public void éteindreLED(int numLED) {
        this.tack.stop();
    }

    @Override
    public void afficherTempo(int valeurTempo) {
        this.displayTempo.setText(String.valueOf(valeurTempo));
    }
}
