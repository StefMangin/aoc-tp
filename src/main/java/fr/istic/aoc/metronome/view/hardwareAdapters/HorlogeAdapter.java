package fr.istic.aoc.metronome.view.hardwareAdapters;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.hardware.component.Command;
import fr.istic.aoc.metronome.hardware.component.Horloge;
import fr.istic.aoc.metronome.view.ControllerFX;

/**
 * Created by pollt on 1/23/16.
 */
public class HorlogeAdapter extends ControllerFX implements Horloge {

    @Override
    public void activerPériodiquement(Command cmd, float périodeEnSecondes) {
        fr.istic.aoc.metronome.utils.Command command = (fr.istic.aoc.metronome.utils.Command) cmd;
        controller.getCore().getClock().addPeriodCmd(command, (long)(périodeEnSecondes * 1000));
        controller.getCore().getClock().start();
    }

    @Override
    public void activerAprèsDélai(Command cmd, float délaiEnSecondes) {
        fr.istic.aoc.metronome.utils.Command command = (fr.istic.aoc.metronome.utils.Command) cmd;
        controller.getCore().getClock().addDelayCmd(command, (long)(délaiEnSecondes * 1000));
        controller.getCore().getClock().start();
    }

    @Override
    public void désactiver(Command cmd) {
        controller.getCore().getClock().stop();
    }
}
