package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.controller.interfaces.IController;

import java.util.Observer;

/**
 *
 * Created by leiko on 28/09/15.
 */
public interface IView {

    /**
     * Called to emit the tack sound
     */
    void onTackSound();

    /**
     * Called to emit the tack sound
     */
    void onTickSound();

    /**
     * Called when the vutton has been pressed
     */
    void onStartPressed();

    /**
     * Indicates that the stop button has been pressed
     */
    void onStopPressed();

    /**
     * Indicates that the increase bpm button has been pressed
     */
    void onIncreaseBpmPressed();

    /**
     * Indicates that the decrease bpm button has been pressed
     */
    void onDecreaseBpmPressed();


    /**
     * Indicates that the start button has been pressed
     * @return
     */
    boolean isStartPressed();

    /**
     * Indicates that the stop button has been pressed
     * @return
     */
    boolean isStopPressed();

    /**
     * Indicates that the increase bpm button has been pressed
     * @return
     */
    boolean isIncreaseBpmPressed();

    /**
     * Indicates that the decrease bpm button has been pressed
     * @return
     */
    boolean isDecreaseBpmPressed();

    /**
     * Called to change tempo display
     */
    void onDisplayChange();

    void onTickLed();

    void onTackLed();


}
