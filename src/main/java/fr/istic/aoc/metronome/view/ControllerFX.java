package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.model.interfaces.ICore;
import javafx.animation.FillTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import sun.applet.AppletAudioClip;

import java.applet.AudioClip;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pollt on 10/6/15.
 */
public class ControllerFX implements IView, Observer {

    //Link to the rest of the app
    protected IController controller;

    //colors
    protected FillTransition tick;
    protected FillTransition tack;
    //sounds
    protected AudioClip kick = new AppletAudioClip(getClass().getResource("/sound/kick.wav"));
    protected AudioClip snare = new AppletAudioClip(getClass().getResource("/sound/snare.wav"));

    @FXML protected Circle ledTick;

    @FXML protected Circle ledTack;

    @FXML protected TextField displayTempo;

    @FXML protected TextField displayBpm;

    @FXML protected Slider tempoSlider;

    @FXML protected Button startButton;

    @FXML protected Button stopButton;

    @FXML protected Button increaseButton;

    @FXML protected Button decreaseButton;

    @FXML
    public void initialize() {

        DropShadow shadow = new DropShadow();

        tick = new FillTransition(Duration.millis(200), ledTick, Color.GREEN, Color.WHITESMOKE);
        tick.setAutoReverse(true);
        tack = new FillTransition(Duration.millis(200), ledTack, Color.ORANGE, Color.WHITESMOKE);
        tack.setAutoReverse(true);
        // Shwo the changing value of the tempo
        tempoSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            tempoSlider.setValueChanging(true);
            displayTempo.setText(String.valueOf((int) tempoSlider.getValue()));
        });
        // BUT send to core only after released
        tempoSlider.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> obs, Boolean wasChanging, Boolean isNowChanging) {
                if (!isNowChanging) {
                    onDisplayChange();
                }
            }
        });
        increaseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onIncreaseBpmPressed();
            }
        });
        decreaseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onDecreaseBpmPressed();
            }
        });
        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onStartPressed();
            }
        });
        stopButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onStopPressed();
            }
        });
        //Adding the shadow when the mouse cursor is on
        startButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        startButton.setEffect(shadow);
                    }
                });
        //Removing the shadow when the mouse cursor is off
        startButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        startButton.setEffect(null);
                    }
                });
        //Adding the shadow when the mouse cursor is on
        tempoSlider.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        tempoSlider.setEffect(shadow);
                    }
                });
        //Removing the shadow when the mouse cursor is off
        tempoSlider.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        tempoSlider.setEffect(null);
                    }
                });
        //Adding the shadow when the mouse cursor is on
        stopButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        stopButton.setEffect(shadow);
                    }
                });
        //Removing the shadow when the mouse cursor is off
        stopButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        stopButton.setEffect(null);
                    }
                });
        //Adding the shadow when the mouse cursor is on
        decreaseButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        decreaseButton.setEffect(shadow);
                    }
                });
        //Removing the shadow when the mouse cursor is off
        decreaseButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        decreaseButton.setEffect(null);
                    }
                });
        //Adding the shadow when the mouse cursor is on
        increaseButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        increaseButton.setEffect(shadow);
                    }
                });
        //Removing the shadow when the mouse cursor is off
        increaseButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
                    @Override public void handle(MouseEvent e) {
                        increaseButton.setEffect(null);
                    }
                });

    }

    public void setController(IController ctrl) {
        this.controller = ctrl;
        // Adjusting slider's and display's value with core value
        tempoSlider.adjustValue((double) ctrl.showTempo());
        displayTempo.setText(String.valueOf(ctrl.showTempo()));
        displayBpm.setText(String.valueOf(ctrl.showBpm()));
        //displayLogs.setText(String.valueOf(ctrl.showLogs()));
    }

    @Override
    public void onTackSound() {
        kick.stop();
        kick.play();
    }

    @Override
    public void onTickSound() {
        snare.stop();
        snare.play();
    }

    @Override
    public void onStartPressed() {
        controller.start();
    }

    @Override
    public void onStopPressed() {
        controller.stop();
    }

    @Override
    public void onIncreaseBpmPressed() {
        controller.increaseBpm();
    }

    @Override
    public void onDecreaseBpmPressed() {
        controller.decreaseBpm();
    }

    @Override
    public boolean isStartPressed() {
        return startButton.isPressed();
    }

    @Override
    public boolean isStopPressed() {
        return stopButton.isPressed();
    }

    @Override
    public boolean isIncreaseBpmPressed() {
        return increaseButton.isPressed();
    }

    @Override
    public boolean isDecreaseBpmPressed() {
        return decreaseButton.isPressed();
    }

    @Override
    public void onDisplayChange() {
        controller.updateTempo((int) tempoSlider.getValue());
    }

    @Override
    public void onTackLed() {
        tack.stop();
        tack.play();
    }

    @Override
    public void onTickLed() {
        tick.stop();
        tick.play();
    }

    public void update(Observable o, Object arg) {
        if (o instanceof ICore) {
            ICore core = (ICore) o;
            Integer tempo = core.getTempo();
            Integer bpm = core.getBpm();
            // Check if user's modifying the value
            // => otherwize core value will be applied to display's and slider's Tempo while adjusting value
            if (!tempoSlider.isValueChanging()) {
                displayTempo.setText(String.valueOf(tempo));
                tempoSlider.adjustValue((double) tempo);
            }
            displayBpm.setText(String.valueOf(bpm));
            //displayLogs.setText(String.valueOf(core.getLogs()));
        }
    }
}
