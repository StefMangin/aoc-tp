package fr.istic.aoc.metronome.view.hardwareAdapters;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.hardware.component.Clavier;
import fr.istic.aoc.metronome.view.ControllerFX;

/**
 * Created by pollt on 1/23/16.
 */
public class ClavierAdapter extends ControllerFX implements Clavier {

    @Override
    public boolean touchePressée(int i) {
        if (i == 1) { // Start
            return startButton.isPressed();
        } else if (i == 2) { // Stop
            return stopButton.isPressed();
        } else if (i == 3) { // Inc
            return increaseButton.isPressed();
        } else if (i == 4) { // Dec
            return decreaseButton.isPressed();
        }
        return false;
    }
}
