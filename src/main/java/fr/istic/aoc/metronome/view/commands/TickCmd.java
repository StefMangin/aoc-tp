package fr.istic.aoc.metronome.view.commands;

import fr.istic.aoc.metronome.controller.interfaces.IController;
import fr.istic.aoc.metronome.utils.Command;
import fr.istic.aoc.metronome.view.ControllerFX;
import fr.istic.aoc.metronome.view.IView;

import java.util.logging.Logger;

/**
 * Created by stephane on 21/10/15.
 */
public class TickCmd implements Command {

    private IView ctrl;
    private Logger log = Logger.getLogger(getClass().getName());

    public TickCmd(IView ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void execute() {
        ctrl.onTickLed();
        ctrl.onTickSound();
        log.finest("tickCmd called.");
    }

    @Override
    public void run() {
        execute();
    }
}
