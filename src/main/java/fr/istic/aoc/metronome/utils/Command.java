package fr.istic.aoc.metronome.utils;

/**
 * Created by stephane on 06/10/15.
 */
public interface Command extends Runnable {

    /** Launch the process
     *
     */
    void execute();

}
