package fr.istic.aoc.metronome.core;

import fr.istic.aoc.metronome.model.Clock;
import fr.istic.aoc.metronome.model.Core;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by stephane on 22/01/16.
 */
public class CoreTest {

    private Integer tempo = 80;
    private Integer tempoMin = 20;
    private Integer tempoMax = 280;
    private Integer bpm = 4;
    private Integer bpmMin = 2;
    private Integer bpmMax = 7;
    private Boolean autostart = false;
    private Core core;

    @Before
    public void setUp() throws Exception {
        Clock clock = mock(Clock.class);
        core = new Core(clock, tempo, bpm, autostart);

    }

    @Test
    public void testGetBpm() throws Exception {
        assertEquals(bpm, core.getBpm());
    }

    @Test
    public void testSetBpm() throws Exception {
        Integer oracle = bpm + 2;
        core.setBpm(oracle);
        assertEquals(oracle, core.getBpm());
    }

    @Test
    public void testGetTempo() throws Exception {
        assertEquals(bpm, core.getBpm());
    }

    @Test
    public void testSetTempo() throws Exception {
        Integer oracle = tempo + 10;
        core.setTempo(oracle);
        assertEquals(oracle, core.getTempo());
    }

    @Test
    public void testSetTempoMin() throws Exception {
        Integer oracle = tempo;
        core.setTempo(tempoMin - 1);
        assertEquals(oracle, core.getTempo());
    }

    @Test
    public void testSetTempoMax() throws Exception {
        Integer oracle = tempo;
        core.setTempo(tempoMax + 1);
        assertEquals(oracle, core.getTempo());
    }

    @Test
    public void testGetOn() throws Exception {
        assertEquals(autostart, core.getOn());
    }

    @Test
    public void testSetOn() throws Exception {
        Boolean oracle = !autostart;
        core.setOn(oracle);
        assertEquals(oracle, core.getOn());
    }


    @Test
    public void testIncreaseBpm() {
        Integer oracle = bpm + 1;
        core.increaseBpm();
        assertEquals(oracle, core.getBpm());
    }

    @Test
    public void testIncreaseBpmMax() {
        Integer oracle = bpmMax;
        core.setBpm(bpmMax);
        core.increaseBpm();
        assertEquals(oracle, core.getBpm());
    }

    @Test
    public void testDecreaseBpm() {
        Integer oracle = bpm - 1;
        core.decreaseBpm();
        assertEquals(oracle, core.getBpm());
    }

    @Test
    public void testDecreaseBpmMin() {
        Integer oracle = bpmMin;
        core.setBpm(bpmMin);
        core.decreaseBpm();
        assertEquals(oracle, core.getBpm());
    }
}