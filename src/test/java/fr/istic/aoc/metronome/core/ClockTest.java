package fr.istic.aoc.metronome.core;

import fr.istic.aoc.metronome.model.Clock;
import fr.istic.aoc.metronome.model.commands.CoreTickCmd;
import fr.istic.aoc.metronome.utils.Command;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by blacknight on 23/01/16.
 */
public class ClockTest extends Clock {

    private ClockTest clock;
    private Long periodTest = 2000L;
    private Command cmdTest;

    @Before
    public void setUp() throws Exception {
        cmdTest = mock(CoreTickCmd.class);
        clock = new ClockTest();
        clock.addPeriodCmd(cmdTest, periodTest);
        assertEquals(false, clock.previousOn);
        assertEquals(periodTest, clock.period);
        assertEquals(cmdTest, clock.cmd);
    }

    @Test
    public void testAddPeriodCmd() throws Exception {
        Long oraclePeriod = 5000L;
        Command oracleCmd = new CoreTickCmd(null);
        clock.addPeriodCmd(oracleCmd, oraclePeriod);
        assertEquals(oraclePeriod, clock.period);
        assertEquals(oracleCmd, clock.cmd);
    }

    @Test
    public void testUpdatePeriod() throws Exception {
        Long oracle = periodTest + 1000L;
        clock.updatePeriod(oracle);
        assertEquals(oracle, clock.period);
        assertNotEquals(null, clock.threadPool);
        assertEquals(false, clock.threadPool.isShutdown());
        assertEquals(false, clock.threadPool.isTerminated());
        assertEquals(cmdTest, clock.cmd);
    }

    @Test
    public void testStart() throws Exception {
        clock.start();
        assertNotEquals(null, clock.threadPool);
        assertEquals(false, clock.threadPool.isShutdown());
        assertEquals(false, clock.threadPool.isTerminated());
        assertEquals(true, clock.previousOn);
        assertEquals(periodTest, clock.period);
        assertEquals(cmdTest, clock.cmd);
    }

    @Test
    public void testStop() throws Exception {
        clock.start();
        clock.stop();
        assertEquals(true, clock.threadPool.isShutdown());
        assertEquals(false, clock.threadPool.isTerminated());
        assertEquals(false, clock.previousOn);
        assertEquals(periodTest, clock.period);
        assertEquals(cmdTest, clock.cmd);
    }
}