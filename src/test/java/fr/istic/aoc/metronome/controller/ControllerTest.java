package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.model.Clock;
import fr.istic.aoc.metronome.model.Core;
import fr.istic.aoc.metronome.utils.Command;
import fr.istic.aoc.metronome.view.ControllerFX;
import fr.istic.aoc.metronome.view.commands.TackCmd;
import fr.istic.aoc.metronome.view.commands.TickCmd;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by blacknight on 23/01/16.
 */
public class ControllerTest {

    private Integer tempo = 80;
    private Integer bpm = 4;
    private Controller ctrl;
    private ControllerFX ctrlFX;
    private Core core;

    @Before
    public void setUp() {
        Clock clock = mock(Clock.class);
        ctrlFX = mock(ControllerFX.class);
        core = new Core(clock, tempo, bpm, false);
        ctrl = new Controller(core);
    }

    @Test
    public void testShowTempo() throws Exception {
        assertEquals(tempo, ctrl.showTempo());
    }

    @Test
    public void testShowBpm() throws Exception {
        assertEquals(bpm, ctrl.showBpm());
    }

    @Test
    public void testUpdateBpm() throws Exception {
        Integer oracle = 5;
        ctrl.updateBpm(oracle);
        assertEquals(oracle, ctrl.showBpm());
    }

    @Test
    public void testUpdateTempo() throws Exception {
        Integer oracle = 220;
        ctrl.updateTempo(oracle);
        assertEquals(oracle, ctrl.showTempo());
    }

    @Test
    public void testIncreaseBpm() throws Exception {
        Integer oracle = bpm + 1;
        ctrl.increaseBpm();
        assertEquals(oracle, ctrl.showBpm());
    }

    @Test
    public void testDecreaseBpm() throws Exception {
        Integer oracle = bpm - 1;
        ctrl.decreaseBpm();
        assertEquals(oracle, ctrl.showBpm());
    }

    @Test
    public void testSetTackCmd() throws Exception {
        Command oracle = new TackCmd(ctrlFX);
        ctrl.setTackCmd(oracle);
        assertEquals(oracle, ctrl.tackCmd);
    }

    @Test
    public void testSetTickCmd() throws Exception {
        Command oracle = new TickCmd(ctrlFX);
        ctrl.setTickCmd(oracle);
        assertEquals(oracle, ctrl.tickCmd);
    }
}