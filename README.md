# Application de métronome

## Conception méthodologique

Pour la conception de ce Metronome, nous avons choisi de developper l'application en suivant un pattern MVC. 
Etant donné que nous travaillons en binome, nous avons décidé de découpé la tache en deux partie pour commencer.

  * IHM (Vue)
  * Coeur métier (Model)

La partie controlleur esquissée une premiere fois, dans une forme minimal. Puis a été augmenté en fonction des besoins fonctionelle des deux autres partie (Vue et Model).

### Implémentation MVC

![AOC-V1_mvc.png](https://bitbucket.org/repo/AA66Gr/images/4121232950-AOC-V1_mvc.png)


### IHM (Vue)

JavaFx a été utilisé pour réaliser l'IHM de l'application. Plus spécifiquement nous avons utilisé le cadriciel Fxml pour avoir un developement rapide et modulaire. Deux versions de l'interface on été produite au totale, la premiere, servant principalement a pouvoir validé le binding Fxml, a été replacer par la version finale, cette derniere plus esthetique.

Cette interface émets des sons à intervales réguliers en marquant les débuts de mesures (__tick__ défini par **Tempo**) ainsi que la subdivision de la mesure (__tack__ défini par **Bpm**).

#### Aperçu de la version finale :

![IHM](https://bitbucket.org/repo/AA66Gr/images/60116814-Sans%20titre.png)

### Coeur métier (Model)

Cette partie réalise toute la partie métier de l'application.

#### Générateur de signal

Une classe ``Clock`` a été créé afin de générer des signaux temporisés permettant de générer les __bips__ nécessaires à la bonne marche de l'application.

#### Traitement des __bip__

La classe ``Core`` envoie au ``Controller`` des signaux dit __bips__ à intervales réguliers. Des commandes ont été spécialement créées pour abstraire la communication entre la ``Clock`` et le ``Core``, le ``Core`` et le ``Controller`` ainsi que le ``ControllerFx`` et le ``Controller``.

#### Implémentation du patron de conception Command pour les envoies de __bips__ (__tick__ et __tack__)

![AOC-V1_Command.png](https://bitbucket.org/repo/AA66Gr/images/2859046727-AOC-V1_Command.png)

#### Implémentation du patron de conception Observateur pour la récupération des données du modèle

![AOC-V1_observer.png](https://bitbucket.org/repo/AA66Gr/images/2969943654-AOC-V1_observer.png)